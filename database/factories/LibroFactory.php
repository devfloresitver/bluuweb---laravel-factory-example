<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Libro;
use Faker\Generator as Faker;
use Carbon\Carbon;
use App\Categoria;


$factory->define(Libro::class, function (Faker $faker) {
	 $cats = App\Categoria::pluck('id')->toArray();
    return [
    	'titulo' => $faker->name,
    	'descripcion' => $faker->sentence,
    	'contenido' => $faker->text,
    	'fecha' =>Carbon::now(),
    	'categoria_id'=> $faker->randomElement($cats)

    ];
});
