<?php

use App\Categoria;
use App\Etiqueta;
use Carbon\Carbon;
use App\Libro;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LibrosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class, 5)->create();
        factory(Categoria::class, 20)->create();
        factory(Etiqueta::class, 200)->create();
        factory(Libro::class, 60)->create()->each(function($libro){
            $netiquetas = rand(3,10);
            for ($i=0; $i < $netiquetas; $i++) { 
                $libro->etiquetas()->attach(Etiqueta::all()->random()->id);
            }
            
        });


        //$libro->etiquetas()->attach([2]);

       // Categoria::truncate();

      /*  $categoria = new Categoria();
        $categoria->nombre = "Categoría 1";
        $categoria->save();

        $categoria = new Categoria();
        $categoria->nombre = "Categoría 2";

        $categoria->save();

    

         //Etiqueta::truncate();

        $etiqueta = new Etiqueta();
        $etiqueta->nombre = "horror";
        $etiqueta->save();

        $etiqueta = new Etiqueta();
        $etiqueta->nombre = "fantasy";
        $etiqueta->save();

        //Libro::truncate();

        $libro = new Libro();
        $libro->titulo = "primer libro";
        $libro->descripcion = "desc 1";
        $libro->contenido ="<p>Resument 1</p>";
        $libro->fecha = Carbon::now();
        $libro->categoria_id = 1;
        $libro->save();
        $libro->etiquetas()->attach([1,2]);


        $libro = new Libro();
        $libro->titulo = "segundo libro";
        $libro->descripcion = "desc 2";
        $libro->contenido = "<p>Resument 2</p>";
        $libro->fecha = Carbon::now();
        $libro->categoria_id = 2;
        $libro->save();

        $libro->etiquetas()->attach([2]);

        $libro = new Libro();
        $libro->titulo = "primer libro";
        $libro->descripcion = "desc 1";
        $libro->contenido ="<p>Resument 1</p>";
        $libro->fecha = Carbon::now();
        $libro->categoria_id = 1;
        $libro->save();
        $libro->etiquetas()->attach([1,2]);


        $libro = new Libro();
        $libro->titulo = "segundo libro";
        $libro->descripcion = "desc 2";
        $libro->contenido = "<p>Resument 2</p>";
        $libro->fecha = Carbon::now();
        $libro->categoria_id = 2;
        $libro->save();

        $libro->etiquetas()->attach([2]);


$libro = new Libro();
        $libro->titulo = "primer libro";
        $libro->descripcion = "desc 1";
        $libro->contenido ="<p>Resument 1</p>";
        $libro->fecha = Carbon::now();
        $libro->categoria_id = 1;
        $libro->save();
        $libro->etiquetas()->attach([1,2]);


        $libro = new Libro();
        $libro->titulo = "segundo libro";
        $libro->descripcion = "desc 2";
        $libro->contenido = "<p>Resument 2</p>";
        $libro->fecha = Carbon::now();
        $libro->categoria_id = 2;
        $libro->save();

        $libro->etiquetas()->attach([2]);


$libro = new Libro();
        $libro->titulo = "primer libro";
        $libro->descripcion = "desc 1";
        $libro->contenido ="<p>Resument 1</p>";
        $libro->fecha = Carbon::now();
        $libro->categoria_id = 1;
        $libro->save();
        $libro->etiquetas()->attach([1,2]);


        $libro = new Libro();
        $libro->titulo = "segundo libro";
        $libro->descripcion = "desc 2";
        $libro->contenido = "<p>Resument 2</p>";
        $libro->fecha = Carbon::now();
        $libro->categoria_id = 2;
        $libro->save();

        $libro->etiquetas()->attach([2]);


$libro = new Libro();
        $libro->titulo = "primer libro";
        $libro->descripcion = "desc 1";
        $libro->contenido ="<p>Resument 1</p>";
        $libro->fecha = Carbon::now();
        $libro->categoria_id = 1;
        $libro->save();
        $libro->etiquetas()->attach([1,2]);


        $libro = new Libro();
        $libro->titulo = "segundo libro";
        $libro->descripcion = "desc 2";
        $libro->contenido = "<p>Resument 2</p>";
        $libro->fecha = Carbon::now();
        $libro->categoria_id = 2;
        $libro->save();

        $libro->etiquetas()->attach([2]);


$libro = new Libro();
        $libro->titulo = "primer libro";
        $libro->descripcion = "desc 1";
        $libro->contenido ="<p>Resument 1</p>";
        $libro->fecha = Carbon::now();
        $libro->categoria_id = 1;
        $libro->save();
        $libro->etiquetas()->attach([1,2]);


        $libro = new Libro();
        $libro->titulo = "segundo libro";
        $libro->descripcion = "desc 2";
        $libro->contenido = "<p>Resument 2</p>";
        $libro->fecha = Carbon::now();
        $libro->categoria_id = 2;
        $libro->save();

        $libro->etiquetas()->attach([2]);*/
        
    }


}
