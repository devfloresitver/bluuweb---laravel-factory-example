<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Libro extends Model
{
	protected $dates = ['fecha'];

    public function categoria(){//uno a muchos
    	return $this->belongsTo(Categoria::class);
    }

    public function etiquetas(){//m a m
    	return $this->belongsToMany(Etiqueta::class);
    }
}
